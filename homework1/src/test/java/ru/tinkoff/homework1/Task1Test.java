package ru.tinkoff.homework1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

public class Task1Test {

    private static Stream<Arguments> provideDataForGetListNumbersThatSquaredTest() {
        return Stream.of(
                Arguments.of(List.of(0), List.of(10)),
                Arguments.of(List.of(1), List.of(11)),
                Arguments.of(List.of(3, 4, 5, 6, 7, 8, 9, 10, 11), List.of(19, 59, 74, 91, 110, 131)),
                Arguments.of(List.of(100, 102, 104, 666, 1023, 555), List.of(10_010, 10_414, 1_046_539))
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForGetListNumbersThatSquaredTest")
    public void getListNumbersThatSquaredTest(List<Integer> integers, List<Integer> expected) {

        List<Integer> result = Task1.getListNumbersThatSquared(integers);

        Assertions.assertIterableEquals(expected, result);
    }
}
