package ru.tinkoff.homework1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Task2Test {

    private static Stream<Arguments> provideDataForGetMapQuantityRepetitionsNonUniqueNumbersTest() {
        return Stream.of(
                Arguments.of(List.of(1, 1, 2, 3, 4, 3, 5, 6), Map.of(1, 2L, 3, 2L)),
                Arguments.of(List.of(1, 2, 3, 4, 5, 6), Map.of())
        );
    }

    static class EntryPojo implements Comparable<EntryPojo> {
        private final Integer key;
        private final Long value;

        public Integer getKey() {
            return key;
        }

        public EntryPojo(Integer key, Long value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EntryPojo entryPojo = (EntryPojo) o;

            if (!key.equals(entryPojo.key)) return false;
            return value.equals(entryPojo.value);
        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }

        @Override
        public int compareTo(EntryPojo o) {
            return Integer.compare(key, o.getKey());
        }

        @Override
        public String toString() {
            return "EntryPojo{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }

    @ParameterizedTest
    @MethodSource("provideDataForGetMapQuantityRepetitionsNonUniqueNumbersTest")
    public void getMapQuantityRepetitionsNonUniqueNumbersTest(List<Integer> integers, Map<Integer, Long> expected) {

        Map<Integer, Long> result = Task2.getMapQuantityRepetitionsNonUniqueNumbers(integers);

        // На этих строчках кода моя жизнь могла прерваться и выкинуть исключение :)
        Assertions.assertIterableEquals(
                expected.entrySet()
                        .stream()
                        .map(e -> new EntryPojo(e.getKey(), e.getValue()))
                        .sorted().toList(),
                result.entrySet()
                        .stream()
                        .map(e -> new EntryPojo(e.getKey(), e.getValue()))
                        .sorted().toList()
        );
    }
}
