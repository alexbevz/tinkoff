package ru.tinkoff.homework1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import ru.tinkoff.homework1.task3.Task3;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

public class Task3Test {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void multithreadedOrderOutputTest1() throws InterruptedException {
        Task3 task = new Task3();

        task.executeMultiThreadOrderOutput(List.of("1", "2", "3", "4", "5", "6"), 3);

        Assertions.assertEquals("123456123456123456", outContent.toString());
    }

    @Test
    public void multithreadedOrderOutputTest2() throws InterruptedException {
        Task3 task = new Task3();

        task.executeMultiThreadOrderOutput(List.of("q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]"), 2);

        Assertions.assertEquals("qwertyuiop[]qwertyuiop[]", outContent.toString());
    }

    @Test
    public void multithreadedOrderOutputTest3() throws InterruptedException {
        Task3 task = new Task3();

        task.executeMultiThreadOrderOutput(List.of("1"), 19);

        Assertions.assertEquals("1111111111111111111", outContent.toString());
    }

    @Test
    public void multithreadedOrderOutputTest4() throws InterruptedException {
        Task3 task = new Task3();

        task.executeMultiThreadOrderOutput(List.of("666", "MMM"), 10);

        Assertions.assertEquals("666MMM666MMM666MMM666MMM666MMM666MMM666MMM666MMM666MMM666MMM", outContent.toString());
    }

    @Test
    public void multithreadedOrderOutputTest5() throws InterruptedException {
        Task3 task = new Task3();

        task.executeMultiThreadOrderOutput(List.of("1", "2", "3"), 6);

        Assertions.assertEquals("123123123123123123", outContent.toString());
    }
}