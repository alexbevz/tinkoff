package ru.tinkoff.homework1;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task2 {

    public static Map<Integer, Long> getMapQuantityRepetitionsNonUniqueNumbers(List<Integer> list) {
        return list.stream()
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() != 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
