package ru.tinkoff.homework1;

import java.util.List;

public class Task1 {

    /**
     * Это решение сработает только с неотрицательными числами (в задание числа от 1 до 100)
     */
    public static List<Integer> getListNumbersThatSquared(List<Integer> list) {
        return list.stream()
                .filter(i -> i % 10 != 4 && i % 10 != 5 && i % 10 != 6)
                .map(i -> i * i + 10)
                .toList();
    }
}
