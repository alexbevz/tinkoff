package ru.tinkoff.homework1.task4;

import java.util.Random;

class Producer implements Runnable {

    private final Store store;

    Producer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (true) {
            store.put();
            try {
                Thread.sleep(random.nextInt(0, 2));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
