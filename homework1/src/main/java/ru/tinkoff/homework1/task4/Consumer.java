package ru.tinkoff.homework1.task4;

class Consumer implements Runnable {

    private final Store store;

    Consumer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        while (true) {
            store.get(Thread.currentThread().getName());
        }
    }
}
