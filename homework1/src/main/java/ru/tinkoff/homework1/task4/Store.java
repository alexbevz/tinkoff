package ru.tinkoff.homework1.task4;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

class Store {

    private final Queue<String> consumerQueue = new ArrayBlockingQueue<>(32073);
    private int quantityGoodsInStore;
    private final int maxCapacity;

    public Store(int quantityGoodsInStore, int maxCapacity) {
        this.quantityGoodsInStore = quantityGoodsInStore;
        this.maxCapacity = maxCapacity;
    }

    public void get(String consumer) {
        consumerQueue.add(consumer);
        synchronized (this) {
            while (quantityGoodsInStore < 1 || !consumerQueue.peek().equals(consumer)) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            quantityGoodsInStore--;
            System.out.println(consumer + " купил 1 товар");
            consumerQueue.poll();
            printCountProducts();

            notifyAll();
        }
    }

    public synchronized void put() {
        while (quantityGoodsInStore >= maxCapacity) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        quantityGoodsInStore++;
        System.out.println("Производитель добавил 1 товар");
        printCountProducts();

        notifyAll();
    }

    private void printCountProducts() {
        System.out.println("Товаров на складе: " + quantityGoodsInStore);
    }
}
