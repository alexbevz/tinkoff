package ru.tinkoff.homework1.task4;

public class Task4 {

    public static void simulateDistributionGoods() throws InterruptedException {
        Store store = new Store(0, 10);

        Producer producer = new Producer(store);
        new Thread(producer).start();

        Consumer consumer1 = new Consumer(store);
        Consumer consumer2 = new Consumer(store);
        Consumer consumer3 = new Consumer(store);
        Consumer consumer4 = new Consumer(store);

        new Thread(consumer1).start();
        Thread.sleep(10);
        new Thread(consumer2).start();
        new Thread(consumer3).start();
        Thread.sleep(1000);
        new Thread(consumer4).start();
    }
}


