package ru.tinkoff.homework1.task3;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class Task3 {

    public void executeMultiThreadOrderOutput(final List<String> messages, final int countRepeated) throws InterruptedException {
        if (messages.size() == 0) {
            throw new EmptyStackException();
        }

        /*
          Создаем экземпляр класса и инициализируем сообщения, которые будем выводить.
          Если реализовывать без инициализации, то сообщения будут выводиться не в правильном порядке.
          Связано с тем, что потоки начинают выполняться в разные время, следовательно,
          последовательность будет искажаться.
         */
        MultiThreadOrderOutput concatenation = new MultiThreadOrderOutput(messages);

        List<Thread> threads = new ArrayList<>();
        for (String message : messages) {
            threads.add(new Thread(() -> {
                try {
                    concatenation.printMessagesByOrder(message, countRepeated);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }));
        }

        for (Thread thread : threads) {
            thread.start();
        }

        /*
          Ждем выполнение всех запущенных потоков, чтобы протестировать вывод.
          В теории не надо писать так, если не уверен, что потоки не умрут.
          Потому что может основной поток встать.
          Но можно задать сколько будет выполняться поток, но тут может возникнуть ситуация,
          что поток не окончил все процедуры до конца.
         */
        for (Thread thread : threads) {
            thread.join();
        }
    }
}
