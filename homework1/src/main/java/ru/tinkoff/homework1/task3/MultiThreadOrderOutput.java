package ru.tinkoff.homework1.task3;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;


public class MultiThreadOrderOutput {
    private final Queue<String> stringQueue = new ArrayBlockingQueue<>(32057);
    private final Object lock = new Object();

    public MultiThreadOrderOutput(List<String> messagesList) {
        this.stringQueue.addAll(messagesList);
    }

    /**
     * Осуществляет вывод по заданному порядку в конструкторе.
     * Выбрана была такая реализация, потому что было важно оставить изначальный порядок.
     *
     * @param message       Сообщение для вывода
     * @param countRepeated Число сообщений
     * @throws InterruptedException
     */
    public void printMessagesByOrder(final String message, final int countRepeated) throws InterruptedException {
        for (int i = 0; i < countRepeated; i++) {
            synchronized (lock) {
                waitQueue(message);
                System.out.print(message);
                stringQueue.poll();
                stringQueue.add(message);
                lock.notifyAll();
            }
        }
    }

    public void waitQueue(String message) throws InterruptedException {
        while (!stringQueue.peek().equals(message)) {
            lock.wait();
        }
    }
}
