package ru.tinkoff.tictactoe.game.element;

public class GameField {

    private final int size;
    private final int[][] field;
    private int quantityEmptyCells;
    private Point lastPoint;
    private Mark lastMark;

    public GameField(final int size) {
        this.size = size;
        this.field = new int[size][size];
        this.quantityEmptyCells = size * size;
    }

    public Mark getLastNote() {
        return lastMark;
    }

    public int getSize() {
        return size;
    }

    public void printField() {
        for (int i = 0; i < size * 2 - 1; i++) {
            for (int j = 0; j < size * 2 - 1; j++) {
                if (i % 2 == 0 && j % 2 == 0) {
                    System.out.print(Mark.values()[field[i / 2][j / 2]].getValue());
                } else if (i % 2 == 1 && j % 2 == 1) {
                    System.out.print("+");
                } else if (i % 2 == 1) {
                    System.out.print("-");
                } else {
                    System.out.print("|");
                }
            }
            System.out.println();
        }
    }

    public boolean isCellOccupied(int x, int y) throws ArrayIndexOutOfBoundsException {
        return field[y][x] != 0;
    }

    public void putNote(int x, int y, Mark mark) throws ArrayIndexOutOfBoundsException {
        if (mark.equals(Mark.EMPTY)) {
            field[y][x] = 0;
            quantityEmptyCells++;
            return;
        }
        if (isCellOccupied(x, y)) {
            throw new RuntimeException("Ячейка занята!");
        }
        if (quantityEmptyCells - 1 < 0) {
            throw new RuntimeException("Ходов больше нет.");
        }

        quantityEmptyCells--;
        field[y][x] = mark.ordinal();
        lastPoint = new Point(y, x);
        lastMark = mark;
    }

    public boolean isExistedFullPaintedLineByLastTurn() {
        boolean isExisted;

        isExisted =
            isFullPaintedRowVector(lastPoint, lastMark) || isFullPaintedColumnVector(lastPoint,
                lastMark);
        if (isOnDiagonal(lastPoint)) {
            isExisted = isExisted || isFullPaintedDiagonalVector(lastMark);
        }
        if (isOnSideDiagonal(lastPoint)) {
            isExisted = isExisted || isFullPaintedSideDiagonalVector(lastMark);
        }
        return isExisted;
    }

    private boolean isFullPaintedRowVector(Point point, Mark mark) {
        for (int i = 0; i < size; i++) {
            if (field[i][point.getY()] != mark.ordinal()) {
                return false;
            }
        }
        return true;
    }

    private boolean isFullPaintedColumnVector(Point point, Mark mark) {
        for (int i = 0; i < size; i++) {
            if (field[point.getX()][i] != mark.ordinal()) {
                return false;
            }
        }
        return true;
    }

    private boolean isFullPaintedDiagonalVector(Mark mark) {
        for (int i = 0; i < size; i++) {
            if (field[i][i] != mark.ordinal()) {
                return false;
            }
        }
        return true;
    }

    private boolean isFullPaintedSideDiagonalVector(Mark mark) {
        for (int i = 0; i < size; i++) {
            if (field[size - i - 1][i] != mark.ordinal()) {
                return false;
            }
        }
        return true;
    }

    private boolean isOnDiagonal(Point point) {
        return point.getX() == point.getY();
    }

    private boolean isOnSideDiagonal(Point point) {
        return point.getX() + point.getY() == size - 1;
    }

    public boolean hasEmptyCells() {
        return quantityEmptyCells > 0;
    }
}
