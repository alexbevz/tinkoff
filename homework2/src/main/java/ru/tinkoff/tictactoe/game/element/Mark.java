package ru.tinkoff.tictactoe.game.element;

public enum Mark {

    EMPTY(' '), X('x'), O('o');

    final private char value;

    Mark(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
}
