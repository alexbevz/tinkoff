package ru.tinkoff.tictactoe.game.player;

import ru.tinkoff.tictactoe.game.element.GameField;
import ru.tinkoff.tictactoe.game.element.Mark;
import ru.tinkoff.tictactoe.game.element.Point;

public class Bot implements TicTacToePlayer {

    /**
     * Искусственный интеллект играет за счет алгоритма минимакс. Проходимся по всем свободным
     * ячейкам и высчитываем наилучший коэффициент.
     *
     * @param gameField поле, на котором идет игра
     * @param mark      какой пометкой играет данный игрок
     */
    @Override
    public void makeMove(GameField gameField, Mark mark) {
        Point bestMove = new Point(-1, -1);

        int bestValue = Integer.MIN_VALUE;
        for (int row = 0; row < gameField.getSize(); row++) {
            for (int col = 0; col < gameField.getSize(); col++) {
                if (!gameField.isCellOccupied(row, col)) {
                    gameField.putNote(row, col, mark);
                    int moveValue = MiniMax.calculate(
                        gameField,
                        mark,
                        false,
                        new MiniMax.MiniMaxMetrics(MiniMax.MAX_DEPTH, Integer.MIN_VALUE,
                            Integer.MAX_VALUE)
                    );
                    gameField.putNote(row, col, Mark.EMPTY);
                    if (moveValue > bestValue) {
                        bestMove = new Point(row, col);
                        bestValue = moveValue;
                    }
                }
            }
        }
        gameField.putNote(bestMove.getX(), bestMove.getY(), mark);
    }

    public static class MiniMax {

        private static final int MAX_DEPTH = 6;

        /**
         * Рассчитывает шанс победы в зависимости от определенных действий.
         *
         * @param gameField игровое поле
         * @param mark      метка, которой играет бот
         * @param isMax     игрок максимизации или минимизации
         * @return коэффициент
         */
        public static int calculate(GameField gameField, Mark mark, boolean isMax,
            MiniMaxMetrics metrics) {
            int boardValue = evaluateChances(gameField, mark, metrics.depth());

            if (isFinishedCalculate(boardValue, metrics, gameField)) {
                return boardValue;
            }
            return isMax ?
                calculateHighestValue(gameField, mark, metrics)
                : calculateLowestValue(gameField, mark, metrics);
        }

        private static boolean isFinishedCalculate(int boardValue, MiniMaxMetrics metrics,
            GameField gameField) {
            return boardValue != 0 || metrics.depth() == 0 || !gameField.hasEmptyCells();
        }

        private static int calculateHighestValue(GameField gameField, Mark mark,
            MiniMaxMetrics metrics) {
            int alpha = metrics.alpha();
            int highestValue = Integer.MIN_VALUE;

            for (int row = 0; row < gameField.getSize(); row++) {
                for (int col = 0; col < gameField.getSize(); col++) {
                    if (!gameField.isCellOccupied(row, col)) {
                        gameField.putNote(row, col, mark);
                        highestValue = Math.max(
                            highestValue,
                            calculate(
                                gameField,
                                mark,
                                false,
                                new MiniMaxMetrics(metrics.depth() - 1, alpha, metrics.beta)
                            )
                        );
                        gameField.putNote(row, col, Mark.EMPTY);
                        alpha = Math.max(alpha, highestValue);
                        if (alpha >= metrics.beta()) {
                            return highestValue;
                        }
                    }
                }
            }
            return highestValue;
        }

        private static int calculateLowestValue(GameField gameField, Mark mark,
            MiniMaxMetrics metrics) {
            Mark enemyMark = mark.equals(Mark.O) ? Mark.X : Mark.O;
            int beta = metrics.beta();
            int lowestVal = Integer.MAX_VALUE;

            for (int row = 0; row < gameField.getSize(); row++) {
                for (int col = 0; col < gameField.getSize(); col++) {
                    if (!gameField.isCellOccupied(row, col)) {
                        gameField.putNote(row, col, enemyMark);
                        lowestVal = Math.min(
                            lowestVal,
                            calculate(
                                gameField,
                                mark,
                                true,
                                new MiniMaxMetrics(metrics.depth() - 1, metrics.alpha(), beta)
                            )
                        );
                        gameField.putNote(row, col, Mark.EMPTY);
                        beta = Math.min(beta, lowestVal);
                        if (metrics.alpha >= beta) {
                            return lowestVal;
                        }
                    }
                }
            }
            return lowestVal;
        }

        /**
         * Оценивание шансов производим за счет проверки выигрыша с двух сторон. Если текущий игрок
         * побеждает на данном ходу, то шанс возрастает. Если соперник побеждает на данном ходу, то
         * шанс падает. Если никто не побеждает, то шанс остается прежнем.
         *
         * @param gameField игровое поле
         * @param mark      пометка игрока
         * @param depth     глубина прохода алгоритма
         * @return значение выгоды
         */
        private static int evaluateChances(GameField gameField, Mark mark, int depth) {
            if (gameField.getLastNote().equals(mark)
                && gameField.isExistedFullPaintedLineByLastTurn()) {
                return depth + 10;
            } else if (!gameField.getLastNote().equals(mark)
                && gameField.isExistedFullPaintedLineByLastTurn()) {
                return -10 - depth;
            } else {
                return 0;
            }
        }

        /**
         * @param depth глубина прохода для алгоритма
         * @param alpha лучшая альтернатива для максимизации
         * @param beta  лучшая альтернатива для минимизации
         */
        record MiniMaxMetrics(int depth, int alpha, int beta) {

        }
    }
}
