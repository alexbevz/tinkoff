package ru.tinkoff.tictactoe.game.player;

import java.util.Scanner;
import ru.tinkoff.tictactoe.game.element.GameField;
import ru.tinkoff.tictactoe.game.element.Mark;

public class User implements TicTacToePlayer {

    @Override
    public void makeMove(GameField gameField, Mark mark) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            gameField.printField();
            System.out.print("""
                Введите координаты клетки.
                Сначала по оси абсцисс, потом по оси ординат.
                Отсчет начинается с 1:
                """);
            try {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                gameField.putNote(x - 1, y - 1, mark);
                break;
            } catch (NumberFormatException e) {
                System.out.print("Неверный ввод!");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.print("Данной ячейки не существует!");
            } catch (RuntimeException e) {
                System.out.print(e.getMessage());
            }
            System.out.println(" Повторите попытку.");
        }
    }
}
