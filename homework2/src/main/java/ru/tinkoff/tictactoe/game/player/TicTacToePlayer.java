package ru.tinkoff.tictactoe.game.player;

import ru.tinkoff.tictactoe.game.element.GameField;
import ru.tinkoff.tictactoe.game.element.Mark;

public interface TicTacToePlayer {

    /**
     * @param gameField поле на котором идет игра
     * @param mark      какой пометкой играет данный игрок
     */
    void makeMove(GameField gameField, Mark mark);
}
