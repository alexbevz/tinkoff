package ru.tinkoff.tictactoe.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import ru.tinkoff.tictactoe.game.element.GameField;
import ru.tinkoff.tictactoe.game.element.Mark;
import ru.tinkoff.tictactoe.game.player.Bot;
import ru.tinkoff.tictactoe.game.player.TicTacToePlayer;
import ru.tinkoff.tictactoe.game.player.User;

public class TicTacToeManager {

    /**
     * Настройка игры.
     */
    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Размеры доски (пример ввода: 3):");
        int n = scanner.nextInt();

        List<TicTacToePlayer> ticTacToePlayers = new ArrayList<>();
        ticTacToePlayers.add(new Bot());
        ticTacToePlayers.add(new User());
        Collections.shuffle(ticTacToePlayers);

        playForTwoPlayers(new GameField(n), ticTacToePlayers.get(0), ticTacToePlayers.get(1));
    }

    /**
     * Осуществление игры, пока кто-то не выиграет или не будет ничья.
     *
     * @param gameField игровое поле
     * @param player1   игрок №1 участвующий в игре
     * @param player2   игрок №2 участвующий в игре
     */
    public void playForTwoPlayers(GameField gameField, TicTacToePlayer player1,
        TicTacToePlayer player2) {

        Map<TicTacToePlayer, Mark> playerMarkMap = Map.of(player1, Mark.X, player2, Mark.O);
        Queue<TicTacToePlayer> playerQueue = new LinkedList<>(List.of(player1, player2));

        TicTacToePlayer curPlayer;
        while (true) {
            curPlayer = playerQueue.poll();
            curPlayer.makeMove(gameField, playerMarkMap.get(curPlayer));
            if (isGameFinished(gameField)) {
                break;
            }
            playerQueue.add(curPlayer);
        }
    }

    /**
     * Проверка на законченность игры.
     *
     * @param gameField игровое поле
     * @return игра закончилась или нет
     */
    public boolean isGameFinished(GameField gameField) {
        if (gameField.isExistedFullPaintedLineByLastTurn()) {
            gameField.printField();
            System.out.println("Победитель: " + gameField.getLastNote().getValue());
        } else if (!gameField.hasEmptyCells()) {
            gameField.printField();
            System.out.println("Ничья");
        } else {
            return false;
        }
        return true;
    }
}
