package ru.tinkoff.tictactoe;

import ru.tinkoff.tictactoe.game.TicTacToeManager;

public class TicTacToe {

    public static void main(String[] args) {
        TicTacToeManager ticTacToeManager = new TicTacToeManager();
        ticTacToeManager.run();
    }
}
